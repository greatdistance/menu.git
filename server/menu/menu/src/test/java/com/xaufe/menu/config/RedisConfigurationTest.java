package com.xaufe.menu.config;

import com.xaufe.menu.BaseSpringTest;
import com.xaufe.menu.service.MenuService;
import com.xaufe.menu.view.MenuView;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;

public class RedisConfigurationTest extends BaseSpringTest {

    @Autowired
    private RedisConnectionFactory factory;

    @Resource
    private MenuService menuService;

    @Autowired
    @Qualifier("menuListTemplate")
    RedisTemplate<String, Object> menuListTemplate;

    @Test
    public void testRedis(){
        //得到一个连接
        MenuView mv = menuService.initMenuView();
        menuListTemplate.opsForValue().set("myMenu",mv);
    }
}
