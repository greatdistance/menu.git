package com.xaufe.menu.view;

import com.xaufe.menu.entity.Menu;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 菜单视图
 *
 */
public class MenuView implements Serializable {

    private List<Menu> topMenuList; // 顶级菜单

    private Map<String,List<Menu>> menuMap;// 顶级菜单及其对应的子菜单

    public List<Menu> getTopMenuList() {
        return topMenuList;
    }

    public void setTopMenuList(List<Menu> topMenuList) {
        this.topMenuList = topMenuList;
    }

    public Map<String, List<Menu>> getMenuMap() {
        return menuMap;
    }

    public void setMenuMap(Map<String, List<Menu>> menuMap) {
        this.menuMap = menuMap;
    }

    @Override
    public String toString() {
        return "MenuView{" +
                "topMenuList=" + topMenuList +
                ",\n menuMap=" + menuMap +
                '}';
    }
}
