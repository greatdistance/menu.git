package com.xaufe.menu.service;

import com.xaufe.menu.entity.Menu;
import com.xaufe.menu.repository.MenuRepository;
import com.xaufe.menu.view.MenuView;

import java.util.List;

public interface MenuService extends BaseService<Menu, Integer, MenuRepository> {

    /**
     * 初始化菜单视图
     *
     * @return MenuView
     */
    MenuView initMenuView();


    /**
     * 获取所有一级菜单集合
     *
     * @return List<Menu>
     */
    List<Menu> findAllTopMenuList();


    /**
     * 获取所有二级菜单集合
     *
     * @param pid 父id
     * @return List<Menu>
     */
    List<Menu> findAllSubMenuList(Integer pid);
}
