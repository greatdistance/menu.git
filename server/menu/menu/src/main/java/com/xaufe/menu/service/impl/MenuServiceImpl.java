package com.xaufe.menu.service.impl;

import com.xaufe.menu.entity.Menu;
import com.xaufe.menu.repository.MenuRepository;
import com.xaufe.menu.service.MenuService;
import com.xaufe.menu.view.MenuView;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class MenuServiceImpl extends BaseServiceImpl<Menu,Integer, MenuRepository> implements MenuService {
    @Override
    public MenuView initMenuView() {
        MenuView mv = new MenuView();
        mv.setTopMenuList(this.findAllTopMenuList());
        HashMap<String,List<Menu>> menuMap = new HashMap<String,List<Menu>>();
        for (Menu menu : mv.getTopMenuList()) {
            menuMap.put(menu.getId()+"",this.findAllSubMenuList(menu.getId()));
        }
        mv.setMenuMap(menuMap);
        return mv;
    }

    @Override
    public List<Menu> findAllTopMenuList() {
        // 规定顶级菜单的pid为-1
        return dao.findAllByPid(-1);
    }

    @Override
    public List<Menu> findAllSubMenuList(Integer pid) {
        return dao.findAllByPid(pid);
    }
}
