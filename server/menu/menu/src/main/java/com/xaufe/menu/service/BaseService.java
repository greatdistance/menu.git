package com.xaufe.menu.service;

import com.xaufe.menu.entity.BaseEntity;
import com.xaufe.menu.repository.BaseRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.List;
/**
 * 封装单表的基本增删改查操作
 */

/**
 * @param <T>  具体的实体类型
 * @param <ID> 实体类型的主键类型
 * @param <R>  该类型的Respoitory的实现类型
 */
public interface BaseService<T extends BaseEntity, ID extends Serializable, R extends BaseRepository<T, ID>> {
    // 1.查询所有
    List<T> findAll();

    // 2.根据id查询单个对象
    T getOne(ID id);

    // 3.分页查询所有数据
    Page<T> findAll(Pageable pageable);

    // 4.添加单个对象
    void save(T obj);

    // 5.根据id修改单个对象
    void update(T obj);

    // 6.根据id删除单个对象
    void deleteById(ID id);
}
