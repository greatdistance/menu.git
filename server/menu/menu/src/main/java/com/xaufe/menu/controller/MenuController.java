package com.xaufe.menu.controller;

import com.xaufe.menu.service.MenuService;
import com.xaufe.menu.utils.Json;
import com.xaufe.menu.view.MenuView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping(value = "menu", produces = "application/json")
public class MenuController {
    @Resource
    private MenuService menuService;

    @Autowired
    @Qualifier("menuListTemplate")
    RedisTemplate<String, Object> menuListTemplate;

    /**
     * 初始化菜单
     *
     * @return Json
     */
    @GetMapping(value = "initMenu")
    public Map<String, Object> initMenuList() {
        MenuView mv = new MenuView();
        if (menuListTemplate.hasKey("myMenu")) {

            // redis中有数据 则在redis中读取菜单
            System.out.println("读取缓存菜单资料");
            mv = (MenuView) menuListTemplate.opsForValue().get("myMenu");
        } else {

            // redis中没有数据 则在数据库中读取
            System.out.println("从数据库中读取菜单资料");
            mv = menuService.initMenuView();

            // 从数据库中读取数据放入redis缓存
            menuListTemplate.opsForValue().set("myMenu", mv);

        }
        if (mv != null) {
            // 查询出结果
            return Json.success(mv);
        } else {
            // 查询结果出错
            return Json.fail();
        }

    }
}
