package com.xaufe.menu.service.impl;

import com.xaufe.menu.entity.BaseEntity;
import com.xaufe.menu.repository.BaseRepository;
import com.xaufe.menu.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

public class BaseServiceImpl<T extends BaseEntity, ID extends Serializable, R extends BaseRepository<T, ID>> implements BaseService<T, ID, R> {

    @Autowired
    protected R dao;

    @Transactional
    @Override
    public List<T> findAll() {
        return dao.findAll();
    }

    @Transactional
    @Override
    public T getOne(ID id) {
        return (T) dao.getOne(id);
    }

    @Transactional
    @Override
    public Page<T> findAll(Pageable pageable) {
        return dao.findAll(pageable);
    }

    @Transactional
    @Override
    public void save(T obj) {
        dao.save(obj);
    }

    @Transactional
    @Override
    public void update(T obj) {
        dao.save(obj);
    }

    @Transactional
    @Override
    public void deleteById(ID id) {
        dao.deleteById(id);
    }
}
