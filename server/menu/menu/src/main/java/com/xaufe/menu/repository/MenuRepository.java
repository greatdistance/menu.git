package com.xaufe.menu.repository;

import com.xaufe.menu.entity.Menu;

import java.util.List;

public interface MenuRepository extends BaseRepository<Menu, Integer> {
    // 1.通过pid查询

    /**
     * 通过pid查询Menu
     *
     * @param pid 父id
     * @return List<Menu>
     */
    List<Menu> findAllByPid(Integer pid);
}
