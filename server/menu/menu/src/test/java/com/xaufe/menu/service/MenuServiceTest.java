package com.xaufe.menu.service;

import com.xaufe.menu.BaseSpringTest;
import com.xaufe.menu.entity.Menu;
import com.xaufe.menu.view.MenuView;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

public class MenuServiceTest extends BaseSpringTest {

    @Resource
    private MenuService menuService;

    @Test
    public void testFindAll() {
        List<Menu> menuList = menuService.findAll();
        for (Menu m : menuList) {
            System.out.println(m);
        }
    }

    @Test
    public void testGetOne() {
        Menu menu1 = menuService.getOne(1);
        Menu menu2 = menuService.getOne(2);
        Menu menu3 = menuService.getOne(3);
        Menu menu4 = menuService.getOne(4);
        Menu menu5 = menuService.getOne(5);
        System.out.println(menu1);
        System.out.println(menu2);
        System.out.println(menu3);
        System.out.println(menu4);
        System.out.println(menu5);

    }

    @Test
    public void testUpdate() {
        Menu menu = menuService.getOne(1);
        menu.setName("一级栏目一");
        menuService.update(menu);
    }
    @Test
    public void testSave() {
        Menu menu = new Menu();
        menu.setName("余永涛");
        menu.setPid(3);
        menuService.save(menu);
    }

    @Test
    public void testDeleteById(){
        menuService.deleteById(16);
    }

    @Test
    public void testFindAllTopMenuList(){
       List<Menu> menuList= menuService.findAllTopMenuList();
        for (Menu menu : menuList) {
            System.out.println(menu);
        }
    }

   @Test
    public void testFindAllSubMenuList(){
       List<Menu> menuList= menuService.findAllSubMenuList(1);
       for (Menu menu : menuList) {
           System.out.println(menu);
       }
   }

   @Test
    public void testInitMenuView(){
       MenuView mv = menuService.initMenuView();
       System.out.println(mv);
   }
}
