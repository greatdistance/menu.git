package com.xaufe.menu.entity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class BaseEntity {

    protected Integer version; //乐观锁
    @Column(length = 32)
    protected String createTime; //表示该记录的创建时间
    @Column(length = 32)
    protected String modifyTime; //表示该记录的最后一次修改时间

    @Column(insertable = false, columnDefinition = "boolean default false")
    protected boolean flag; //表示该记录是否是已经被删除的记录。


    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "version=" + version +
                ", createTime='" + createTime + '\'' +
                ", modifyTime='" + modifyTime + '\'' +
                ", flag=" + flag +
                '}';
    }
}
